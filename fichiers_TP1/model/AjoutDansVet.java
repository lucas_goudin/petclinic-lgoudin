
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vet")
    private Set<Memo> memos;

    
    protected Set<Memo> getMemosInternal() {
        if (this.memos == null) {
            this.memos = new HashSet<>();
        }
        return this.memos;
    }

    protected void setMemosInternal(Set<Memo> memos) {
        this.memos = memos;
    }

    public List<Memo> getMemos() {
        List<Memo> sortedMemos = new ArrayList<>(getMemosInternal());
        PropertyComparator.sort(sortedMemos, new MutableSortDefinition("date", true, true));
        return Collections.unmodifiableList(sortedMemos);
    }

    public void addMemo(Memo memo) {
        getMemosInternal().add(memo);
        memo.setVet(this);
    }
    
    public int getNrOfMemos() {
        return getMemosInternal().size();
    }

