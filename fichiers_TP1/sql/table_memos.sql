CREATE TABLE memos (
  id          INTEGER IDENTITY PRIMARY KEY,
  vet_id      INTEGER NOT NULL,
  memo_date  DATE,
  description VARCHAR(255)
);
ALTER TABLE memos ADD CONSTRAINT fk_memos_vets FOREIGN KEY (vet_id) REFERENCES vets (id);
CREATE INDEX memos_vet_id ON memos (vet_id);
